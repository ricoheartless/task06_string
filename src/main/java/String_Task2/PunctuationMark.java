package String_Task2;

import java.util.Objects;

public class PunctuationMark implements SentenceInterface {

  private char mark;

  PunctuationMark(char mark) {
    this.mark = mark;
  }

  public char getMark() {
    return mark;
  }

  public static boolean isPunctuationMark(String s) {
    if (s.length() == 1) {
      char c = s.charAt(0);
      if (c == '?' || c == '!' || c == ',' || c == '.' || c == ' ') {
        return true;
      }
    }
    return false;
  }

  @Override
  public String getElement() {
    return Character.toString(mark);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PunctuationMark that = (PunctuationMark) o;
    return mark == that.mark;
  }

  @Override
  public int hashCode() {
    return Objects.hash(mark);
  }

  @Override
  public int length() {
    return 1;
  }

  @Override
  public boolean isVowel() {
    return "AEIOUaeiou".indexOf(this.getElement().charAt(0)) != -1;
  }

  @Override
  public void set(String string) {
    this.mark = string.charAt(0);
  }
}