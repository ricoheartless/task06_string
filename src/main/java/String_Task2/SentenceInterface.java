package String_Task2;

public interface SentenceInterface {

  String getElement();

  int length();

  boolean isVowel();

  void set(String string);

}
