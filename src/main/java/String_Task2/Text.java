package String_Task2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Text {
  private String text;
  private List<Sentence> sentenceList;
  private String sentencePattern = "[A-Z][A-z\\,\\'\\s]+[\\.,\\?,\\!]"; //Sentence regex
  private Pattern pattern = Pattern.compile(sentencePattern);

  Text(String text){
    this.text = text;
    sentenceList = new ArrayList<>();
    Matcher matcher = pattern.matcher(text);
    while (matcher.find()){
      sentenceList.add(new Sentence(text.substring(matcher.start(),matcher.end())));
    }
  }

  public String getText() {
    return text;
  }

  public void sortAndShowByWords() {
    Map<String, Integer> map = new HashMap<>();
    for (Sentence s : this.getSentenceList()) {
      Integer wordCount = s.countWords();
      map.put(s.toString(), wordCount);
    }
    map.entrySet().stream()
        .sorted(Map.Entry.comparingByValue())
        .map(e -> e.getKey())
        .forEach(System.out::println);
  }

   public void showChangeByVowel() {
    Word longest;
    List<Sentence> list = new ArrayList<>(this.getSentenceList());
     for (Sentence sentence:this.getSentenceList()) {
       longest = (Word)sentence.getLongestWord();
       if(sentence.getListElements().get(0).isVowel()){ //If sentence starts with vowel
         int indexOfLongest = sentence.getListElements().indexOf(longest); //sets index of the longest word
         Word firstWord = (Word)sentence.getListElements().get(0); //sets first word
         int sentenceIndex = this.getSentenceList().indexOf(sentence); //sets index of sentence
         Sentence inputSentence = this.getSentenceList().get(sentenceIndex); //Sets whole sentence
         List elementsList = new ArrayList<>(inputSentence.getListElements()); //Sets list
         longest = longest.firstLetterToUpperCase(); //Change first letter to uppercase
         firstWord = firstWord.toLowerCase(); //Change word to lowercase
         elementsList.set(0, longest);
         elementsList.set(indexOfLongest, firstWord);
         inputSentence.set(elementsList);
         list.set(sentenceIndex,inputSentence);
       }
     }
     list.forEach(System.out::println);
  }

  public void showSentencesWMultipleWords() {
    Set<String> set = new HashSet<>();
    List<Sentence> result = new ArrayList<>();
    for (Sentence s : this.getSentenceList()) {
      for (SentenceInterface word : s.getListElements()) {
        if (!PunctuationMark.isPunctuationMark(word.getElement())) { //Checks if it's word or not
          if(!set.add(word.getElement())) {
            result.add(s);
          }
        }
      }
    }
    result.forEach(System.out::println);
  }

  public void showQuestionWord(int length) {
    List<String> words  = new ArrayList<>();
    for (Sentence s:this.getSentenceList()) {
      List <SentenceInterface> elements = s.getListElements();
      if(elements.get(elements.size()-1).getElement().equals("?")){
        for (SentenceInterface w:s.getListElements()) {
          if(w.length() == length){
            words.add(w.getElement());
          }
        }
      }
    }
    words.forEach(System.out::println);
  }

  public String getUniqueFirstSentenceWord() {
    List<SentenceInterface> firstSentenceWords = this.getSentenceList().get(0).getListElements();
    List<SentenceInterface> restWords = new ArrayList<>();
    List<Sentence> list = this.getSentenceList();
    list.remove(0);
    for (Sentence s :list) {
      for (SentenceInterface e: s.getListElements()) {
         restWords.add(e);
      }
    }
    for (SentenceInterface fSW : firstSentenceWords) {
      if (!restWords.contains(fSW)) {
        return fSW.getElement();
      }
    }
    return null;
  }

  public List<Sentence> getSentenceList(){
    return sentenceList;
  }

  public List<String> getAllWords(){
    List<String> words = new ArrayList<>();
    for (Sentence s:sentenceList) {
      for (SentenceInterface w:s.getListElements()) {
        if(!PunctuationMark.isPunctuationMark(w.getElement()))
        words.add(w.getElement());
      }
    }
    return words;
  }

  public void showAllWordsSorted() {
    List<String> words = this.getAllWords();
    words = words.stream().map(String::toLowerCase).collect(Collectors.toList());
    Collections.sort(words);
    int diff = words.get(0).charAt(0);
    int count = 0;
    for (String w : words) {
      if (w.charAt(0) > diff) {
        ++count;
        diff = w.charAt(0);
      }
      if (count != 0) {
        for (int i = 0; i < count; i++) {
          System.out.print("\t");
        }
        System.out.println(w);
      } else {
        System.out.println(w);
      }
    }
  }
}
