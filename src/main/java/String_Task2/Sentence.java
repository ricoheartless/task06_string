package String_Task2;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sentence {

  private String sentenceElementeRegex = "[\\w]+|[\\.\\,\\?\\!\\s\\&]"; //Word or Symbol
  private Pattern pattern = Pattern.compile(sentenceElementeRegex);
  private String sentence;
  List<SentenceInterface>sentenceElements;

  Sentence(String sentence){
    this.sentence = sentence;
    sentenceElements = new ArrayList<>();
    Matcher matcher = pattern.matcher(sentence);
    //Слово або пунктуаційний символ
    while (matcher.find()){
      String element = sentence.substring(matcher.start(),matcher.end());
      if(PunctuationMark.isPunctuationMark(element)){
        sentenceElements.add(new PunctuationMark(element.charAt(0)));
      }
      else {
        sentenceElements.add(new Word(element));
      }
    }
  }

  public List<SentenceInterface> getListElements() {
    return sentenceElements;
  }

  @Override
  public String toString(){
    return sentence;
  }

  public int countWords() {
    int count = 0;
    for (SentenceInterface element: this.getListElements()) {
      if(!PunctuationMark.isPunctuationMark(element.getElement())){
        count++;
      }
    }
    return count;
  }

  public SentenceInterface getLongestWord(){
    SentenceInterface longest = new Word("");
    for (SentenceInterface e:this.getListElements()) {
      if(!PunctuationMark.isPunctuationMark(e.getElement())){
        if(e.length() >= longest.length()){
          longest = e;
        }
      }
    }
    return longest;
  }

  public void set(List <SentenceInterface> list){
    this.sentenceElements = list;
    this.sentence = "";
    for (SentenceInterface s:list) {
     this.sentence += s.getElement();
    }
  }
  public void replaceWords(int wordLen, String str) {
    Sentence s = new Sentence(str);
    List <SentenceInterface> list = this.getListElements();
    List <SentenceInterface> oldList = list;
    for (int i = 0; i < oldList.size(); i++) {
      if (!PunctuationMark.isPunctuationMark(oldList.get(i).getElement())) {
        if (oldList.get(i).length() == wordLen) {
          int index = oldList.indexOf(oldList.get(i));
          list.remove(oldList.get(i));
          for (int j = 0; j < s.getListElements().size(); j++) {
            list.add(index + j, s.getListElements().get(j));
          }
        }
      }
    }
    this.set(list);
  }



}
