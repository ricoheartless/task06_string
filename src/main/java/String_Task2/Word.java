package String_Task2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Word implements SentenceInterface{

  private static String regexPattern = "[A-z\\']+";
  public static Pattern pattern = Pattern.compile(regexPattern);
  private String word;

  Word(String word){
    this.word = word;
  }
  public Word firstLetterToUpperCase(){
    Word result;
    result = new Word(Character.toUpperCase(this.word.charAt(0)) + this.word.substring(1));
    return result;
  }

  public Word toLowerCase(){
    return new Word(this.word.toLowerCase());
  }

  public static List<String> getAllWords(String original) {
    Matcher matcher = pattern.matcher(original);
    List<String> words = new ArrayList<>();
    while (matcher.find()) {
      words.add(original.substring(matcher.start(), matcher.end()));
    }
    return words;
  }

  public static List<String> getAllWords(List<String> sentences) {
    List<String> words = new ArrayList<>();
    for (String s : sentences) {
      Matcher matcher = pattern.matcher(s);
      while (matcher.find()) {
        words.add(s.substring(matcher.start(), matcher.end()));
      }
    }
    return words;
  }

  public boolean isVowel() {
    return "AEIOUaeiou".indexOf(this.getElement().charAt(0)) != -1;
  }

  @Override
  public void set(String string) {
    this.word = string;
  }

  public static String getLongestWord(List<String> words) {
    String result = "";
    for (String word : words) {
      if (word.length() >= result.length()) {
        result = word;
      }
    }
    return result;
  }

  public int length(){
    return this.getElement().length();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Word word1 = (Word) o;
    return Objects.equals(word, word1.word);
  }

  @Override
  public int hashCode() {
    return Objects.hash(word);
  }

  @Override
  public String getElement() {
    return word;
  }

  @Override
  public String toString(){
    return word;
  }

  public boolean isPalendrome(){
    return this.getElement().equals(new StringBuilder(this.getElement()).reverse().toString());
  }
}
