package PresentationTasks.StringUtils;

import PresentationTasks.Interface.PrintableInterface;

public class UtilsTest implements PrintableInterface {
  @Override
  public void show() {
    String name = "Dmytro";
    Integer num = 21;
    Double num2 = 1998.0;
    StringUtils<Object> stringUtils = new StringUtils<Object>(name,num,num2);
    System.out.println(stringUtils.getString());
  }
}
